import { api } from "@/lib/axios";

export interface ApporveOrderParams {
  orderId: string;
}

export async function approveOrder({ orderId }: ApporveOrderParams) {
  await api.patch(`/orders/${orderId}/approve`)
}