import { api } from "@/lib/axios";

export interface GetMonthCanceldOrdersAmountResponse {
  amount: number
  diffFromLastMonth: number
}

export async function getMonthCanceledOrdersAmount() {
  const response = await api.get<GetMonthCanceldOrdersAmountResponse>('/metrics/month-canceled-orders-amount')

  return response.data
}