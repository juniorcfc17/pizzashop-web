import { render } from '@testing-library/react'
import { OrderStatus } from './order-status'

describe('Order Status', () => {
  it('should diplay the right text based on order status', () => {
    let wrarpper = render(<OrderStatus status='pending' />)

    // wrarpper.debug()
    const statusText = wrarpper.getByText('Pendente')

    expect(statusText).toBeInTheDocument()
  })
})