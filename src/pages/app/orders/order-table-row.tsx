import { Button } from "@/components/ui/button";
import { Dialog } from "@/components/ui/dialog";
import { TableRow, TableCell } from "@/components/ui/table";
import { DialogTrigger } from "@radix-ui/react-dialog";
import { ArrowRight, Search, X } from "lucide-react";
import { OrderDetails } from "./order-details";
import { OrderStatus } from "@/components/order-status";
import { formatDistanceToNow } from 'date-fns'
import { ptBR } from 'date-fns/locale'
import { useState } from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { cancelOrder } from "@/api/cancel-order";
import { GetOrdersResponse } from "@/api/get-orders";
import { approveOrder } from "@/api/approve-order";
import { deliverOrder } from "@/api/deliver-order";
import { dispatchOrder } from "@/api/dispatch-order";

interface OrderTableRowProps {
  order: {
    orderId: string
    createdAt: string
    status: 'pending' | 'canceled' | 'processing' | 'delivering' | 'delivered'
    customerName: string
    total: number
  }
}

export function OrderTableRow({ order }: OrderTableRowProps) {
  const [isDetailedOpen, setisDetailedOpen] = useState(false)
  const queryClient = useQueryClient()

  function updateOrderStatusOnCache(orderId: string, status: OrderStatus) {
    const ordersListCache = queryClient.getQueriesData<GetOrdersResponse>({
      queryKey: ['orders']
    })

    ordersListCache.forEach(([cacheKey, cacheData]) => {
      if (!cacheData) {
        return
      }
      queryClient.setQueryData<GetOrdersResponse>(cacheKey, {
        ...cacheData,
        orders: cacheData.orders.map(order => {
          if (order.orderId === orderId) {
            return { ...order, status }
          }
          return order
        })
      })
    })
  }

  const { mutateAsync: approveOrderFn, isPending: isApproveOrder } = useMutation({
    mutationFn: approveOrder,
    async onSuccess(_, { orderId }) {
      updateOrderStatusOnCache(orderId, 'processing')
    }
  })

  const { mutateAsync: dispatchOrderFn, isPending: isDispatchOrder } = useMutation({
    mutationFn: dispatchOrder,
    async onSuccess(_, { orderId }) {
      updateOrderStatusOnCache(orderId, 'delivering')
    }
  })

  const { mutateAsync: deliverOrderFn, isPending: isDeliverOrder } = useMutation({
    mutationFn: deliverOrder,
    async onSuccess(_, { orderId }) {
      updateOrderStatusOnCache(orderId, 'delivered')
    }
  })


  const { mutateAsync: cancelOrderFn, isPending: isCancelOrder } = useMutation({
    mutationFn: cancelOrder,
    async onSuccess(_, { orderId }) {
      updateOrderStatusOnCache(orderId, 'canceled')
    }
  })

  return (
    <TableRow>
      <TableCell>
        <Dialog open={isDetailedOpen} onOpenChange={setisDetailedOpen}>
          <DialogTrigger asChild>
            <Button variant='outline' size='sx'>
              <Search className="w-3 h-3" />
              <span className="sr-only">Detalhes do pedido</span>
            </Button>
          </DialogTrigger>

          <OrderDetails open={isDetailedOpen} orderId={order.orderId} />
        </Dialog>
      </TableCell>
      <TableCell className="font-mono text-sm font-medium">{order.orderId}</TableCell>
      <TableCell className="text-muted-foreground">
        {formatDistanceToNow(order.createdAt, {
          locale: ptBR,
          addSuffix: true
        })}
      </TableCell>
      <TableCell>
        <OrderStatus status={order.status} />
      </TableCell>
      <TableCell className="font-medium">{order.customerName}</TableCell>
      <TableCell className="font-medium">{(order.total / 100).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}</TableCell>
      <TableCell>
        {order.status === 'pending' &&
          <Button
            onClick={() => approveOrderFn({ orderId: order.orderId })}
            disabled={isApproveOrder}
            variant='outline'
            size='sx'
          >
            <ArrowRight className="w-3 h-3 mr-2" />
            Aprovar
          </Button>
        }

        {order.status === 'processing' &&
          <Button
            onClick={() => dispatchOrderFn({ orderId: order.orderId })}
            disabled={isDispatchOrder}
            variant='outline'
            size='sx'
          >
            <ArrowRight className="w-3 h-3 mr-2" />
            Em entrega
          </Button>
        }

        {order.status === 'delivering' &&
          <Button
            onClick={() => deliverOrderFn({ orderId: order.orderId })}
            disabled={isDeliverOrder}
            variant='outline'
            size='sx'
          >
            <ArrowRight className="w-3 h-3 mr-2" />
            Entregue
          </Button>
        }
      </TableCell>
      <TableCell>
        <Button
          onClick={() => cancelOrderFn({ orderId: order.orderId })}
          disabled={!['pending', 'processing'].includes(order.status) || isCancelOrder}
          variant='ghost'
          size='sx'>
          <X className="w-3 h-3 mr-2" />
          Cancelar
        </Button>
      </TableCell>
    </TableRow>
  )
}